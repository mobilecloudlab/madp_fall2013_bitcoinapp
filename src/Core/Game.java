package Core;

import java.util.Date;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import DataClasses.Round;
import DataClasses.Session;
import DataClasses.Speculation;
import DataClasses.Ticket;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import Core.Utilities;

import com.example.rpbdesign.MainActivity;

public final class Game {
	public final static String TOKEN = "23659abec896fb3f54ac093a24176bb040d7bf4400ce9e2a5ec12fe305164340";
	final Context context;
	
	public boolean game_started = false;
	public boolean speculation_made = false;

	private Round play_round;
	private Round getPlay_round() {
		return play_round;
	}
	private void setPlay_round(Round play_round) {
		this.play_round = play_round;
	}
	
	private Round live_round;
	private Round getLive_round() {
		return live_round;
	}
	private void setLive_round(Round live_round) {
		this.live_round = live_round;
	}
	
	public List<Speculation> speculations = null;
	public Session session = null;
	
	private String _guid;
	public String getGuid() {
		return _guid;
	}
	public void setGuid(String _guid) {
		this._guid = _guid;
	}
	
	private Ticket _ticket;
	public Ticket getTicket() {
		return _ticket;
	}
	public void setTicket(Ticket _ticket) {
		this._ticket = _ticket;
	}
	
	boolean use_bitcoins  = false;
	public boolean UseBitcoins(){
		return this.use_bitcoins;
	}
	public void setUseBitcoins(){
		this.use_playmoney = false;
		this.use_bitcoins = true;
		if(this.getLive_round() == null) this.GetNextRound();
	}

	boolean use_playmoney = true;
	public boolean UsePlayMoney(){
		return this.use_playmoney;
	}
	public void setUsePlayMoney(){
		this.use_bitcoins = false;
		this.use_playmoney = true;
		if(this.getPlay_round() == null) this.GetNextRound();
	}
	
	public String spec_nr = "";
	public String bet_key = "";

	public String btc_addr = "";
	
	public int pot_balance = 0;
		
	public int btc_balance = 0;
	public int playmoney_balance = 0;
	public boolean client_calc = false;
	private boolean finalized = false;
	public boolean end_time_met = false;
	
	private static Game game = null;

	private Game(String guid, Context context){
		this.setGuid(guid);
		if(this.getGuid() == "") {
			this.setGuid(Utilities.GenerateGUID());
			this.context = context;
			new init().execute(this.getGuid());
		}else{
			this.context = context;
			this.startAfterInit();
		}
	}
	
	//Returns a instance of the game (always the same instance)
	public static Game getInstance(String guid, Context context){
		if(game == null){
			game = new Game(guid, context);
		}
		return game;
	}
	
	private class init extends AsyncTask<String, Void, String> {	  
		@Override
		protected String doInBackground(String... params) {
			String ret = null;
			try {
				ret = RPBClient.init(params[0]);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		    return ret;
		}      
	
		@Override
		protected void onPostExecute(String result) { 
			game.startAfterInit();
		}
	}
	
	public void startAfterInit(){
		game_started = true;
		this.GetNextRound();
		this.populateSession();
	}
	
	public void resetRound(){
    	this.speculation_made = false;
    	this.speculations = null;
    	this.spec_nr = "";
    	this.bet_key = "";   	
    	this.pot_balance = 0;
    	this.client_calc = false;
    	this.finalized = false;
    	this.end_time_met = false;
    	this.setActiveRound(null);
	}
	
	public Round getActiveRound(){
		if(this.use_bitcoins == true){
			return this.getLive_round();
		}else{
			return this.getPlay_round();
		}
	}
	
	public void setActiveRound(Round r){
		if(this.use_bitcoins == true){
			this.setLive_round(r);
		}else{
			this.setPlay_round(r);
		}
	}
	
	public String getGameStatus(){
		if(this.use_bitcoins == true){
			return "1";
		}else{
			return "0";
		}
	}
	
	public void GetNextRound(){
		String input[];
		input = new String[2];
		input[0] = this.getGuid();
		input[1] = this.getGameStatus();
		new GetNextRound().execute(input);
	}
	
	private class GetNextRound extends AsyncTask<String, Void, Round> {
		  
		@Override
		protected Round doInBackground(String... params) {
			Round ret = null;
			try {
				String t = RPBClient.getNextRound(params[0], params[1]);
				ret = new Round(t);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		    return ret;
		}      
	
		@Override
		protected void onPostExecute(Round result) {  
			try {
				if(result != null){
					if(getActiveRound() != null){
						if(result.getRoundNr() != getActiveRound().getRoundNr()){
							resetRound();
						}
					}
					setActiveRound(result);
					pot_balance = getActiveRound().getPotSize();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			       
		}
	}
	
	public void refreshRound(){
		if(this.getActiveRound() != null){
//			if(this.getActiveRound().getKeyTime() != null && !isFinalized()){
//				if(this.getActiveRound().getCurrentTime().compareTo(this.getActiveRound().getKeyTime()) > 0){
//					this.finalizeBet();
//				}
//			}
			if(this.getActiveRound().getEndTime() != null){
				Date end = this.getActiveRound().getEndTime();
				Date cur = this.getActiveRound().getCurrentTime();
				int t = cur.compareTo(end);
				Log.v("GAME", "refresh_round_end_time" + end.toString());
				Log.v("GAME", "refresh_round_current_time" + cur.toString());
				Log.v("GAME", "refresh_round_time_diff" + Integer.toString(t));
				if(this.getActiveRound().getCurrentTime().compareTo(this.getActiveRound().getEndTime()) > 0) {
					this.end_time_met = true;
					if(!client_calc){
						getIntermediateResults();
					}
				}
			}
			new GetRound().execute(new String[]{this.getGuid(), ((Integer)this.getActiveRound().getRoundNr()).toString()});
		}
	}

	public int getIntermediateResults(){
		if(this.getActiveRound() == null) return 0;
		if(speculations == null){ 
			this.setFinalized(true);
			return 0;
		}
		if(game.spec_nr.equals("")){ 
			this.setFinalized(true);
			return 0;
		}
		int ret = 0;
		int my_spec = -1;
		int count_rocks = 0, count_papers = 0, count_btc = 0;
		int pot_rocks = 0, pot_papers = 0, pot_btc=0;
		
		for(Speculation s : this.speculations){
			if(!s.getKey().equals("")){
				if(s.getSpecultaionNr() == Integer.parseInt(game.spec_nr)){
					my_spec = Integer.parseInt(s.getKey().substring(0, 1));
				}
				int spec = Integer.parseInt(s.getKey().substring(0, 1));
				switch(spec){
				case 0:
					count_rocks++;
					break;
				case 1:
					count_papers++;
					break;
				case 2:
					count_btc++;
					break;
				}
			}
		}
		
		pot_rocks = count_btc * this.getActiveRound().getForfeit();
		Log.v("GAME", "pot_rocks=" + pot_rocks);
		pot_papers = count_rocks * this.getActiveRound().getForfeit();
		Log.v("GAME", "pot_papers=" + pot_papers);
		pot_btc = count_papers * this.getActiveRound().getForfeit();
		Log.v("GAME", "pot_btc=" + pot_btc);
		
				
        if (count_rocks == 0) {
        	pot_btc += pot_rocks;    
        	pot_rocks    = 0;
        } 
        if (count_papers == 0) {
        	pot_rocks   += pot_papers;
        	pot_papers   = 0;
        } 
        if (count_btc == 0) {
        	pot_papers  += pot_btc; 
        	pot_btc = 0;
        }
        
        int reward_rock     = 0;                
        if (count_rocks > 0) reward_rock = (int) Math.floor(pot_rocks / count_rocks);             
        Log.v("GAME", "reward_rock=" + reward_rock);
        int reward_paper    = 0;
        if (count_papers > 0) reward_paper = (int) Math.floor(pot_papers / count_papers);
        Log.v("GAME", "reward_paper=" + reward_paper);
        int reward_btc = 0;
        if (count_btc > 0) reward_btc = (int) Math.floor(pot_btc / count_btc);
        Log.v("GAME", "reward_btc=" + reward_btc);
        
        this.client_calc = true;
        switch(my_spec){
		case 0:	
			ret = reward_rock;
			break;
		case 1:	
			ret = reward_paper;
			break;
		case 2: 
			ret = reward_btc;
			break;
		}
        Log.v("GAME", "stake=" + ret);
		((MainActivity)this.context).setStake(ret);
		return ret;
	}
	
	private class GetRound extends AsyncTask<String, Void, Round> {
		  
		@Override
		protected Round doInBackground(String... params) {
			Round ret = null;
			try {
				ret = RPBClient.getRound(params[0], params[1]);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		    return ret;
		}      
	
		@Override
		protected void onPostExecute(Round result) {  
			try {
				if(result != null){
					if(getGameStatus().equals(Integer.toString(result.getLive()))){
						setActiveRound(result);
						pot_balance = getActiveRound().getPotSize();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			       
		}
	}
	
	public void MakeBet(String value, Boolean change_bet, String spec_nr){
		if(!game_started) return;
		if(this.getActiveRound() == null) return;
		if(end_time_met) return;
		String[] input;
		if(change_bet == false){
			input = new String[4];
			input[0] = this.getGuid();
			input[1] = this.getActiveRound().getRoundNr().toString();
			this.bet_key = Utilities.betToKey(value);
			input[2] = bet_key;
			if(this.UsePlayMoney()){
				input[3] = "0";
			}else if(this.UseBitcoins()){
				input[3] = "1";
			}
			Log.v("GAME", "Make bet: " + input[0] +" "+ input[1] +" "+ input[2] +" "+ input[3]);
			new MakeBet().execute(input);
		}else{
			input = new String[4];
			input[0] = this.getGuid();
			input[1] = spec_nr;
			input[2] =  bet_key;
			if(this.UsePlayMoney()){
				input[3] = "0";
			}else if(this.UseBitcoins()){
				input[3] = "1";
			}
			Log.v("GAME", "Change bet: " + input[0] +" "+ input[1] +" "+ input[2] +" "+ input[3]);
			new ChangeBet().execute(input);
		}
	}
	
	private class MakeBet extends AsyncTask<String, Void, String> {
		  
		@Override
		protected String doInBackground(String... params) {
			String ret = null;
			try {
				ret = RPBClient.makeBet(params[0], params[1], params[2], params[3]);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		    return ret;
		}      
	
		@Override
		protected void onPostExecute(String result) { 
			if(result == null) return;
			JSONObject data;
			try {
				data = new JSONObject(result);
				speculation_made = true;
				spec_nr = data.get("speculation_nr").toString();
				game.refreshRound();
			} catch (JSONException e) {
				speculation_made = false;
			}

			       
		}
	}
	
	private class ChangeBet extends AsyncTask<String, Void, Boolean>{
		@Override
		protected Boolean doInBackground(String... params) {
			Boolean ret = false;
			try {
				ret = RPBClient.changeBet(params[0], params[1], params[2]);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		    return ret;
		}      
	
		@Override
		protected void onPostExecute(Boolean result) {  
			try {
				if(result == true) refreshRound();
			} catch (Exception e) {e.printStackTrace();}
		}
	}

	public List<Speculation> getSpeculations(){
		return this.speculations;
	}
	
	public void populateSpeculations(){
		if(this.getGuid() != "" && this.getActiveRound() != null){
			new GetSpeculations().execute(new String[]{this.getGuid(), this.getActiveRound().getRoundNr().toString()});
		}
	}
		
	private class GetSpeculations extends AsyncTask<String, Void, List<Speculation>>{
		@Override
		protected List<Speculation> doInBackground(String... params) {
			List<Speculation> ret = null;
			try {
				ret = RPBClient.getSpeculations(params[0], params[1]);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		    return ret;
		}      
	
		@Override
		protected void onPostExecute(List<Speculation> result) {  
				if(result != null){
					speculations = result;
				}			       
		}
	}
	
	public Session getSession(){
		return this.session;
	}
	
	public void populateSession(){
		new GetSession().execute(this.getGuid());
	}
	
	private class GetSession extends AsyncTask<String, Void, Session>{

		@Override
		protected Session doInBackground(String... params) {
			Session ret = null;
			try {
				ret = RPBClient.getSession(params[0]);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		    return ret;
		}

		@Override
		protected void onPostExecute(Session result) { 
			try {
			session = result;
			if(session != null) {
				playmoney_balance = session.getTestBalance_Combined();
				btc_balance = session.getBTCBalance_Combined();
				
				Log.v("GAME", Integer.toString(playmoney_balance));
				Log.v("GAME", Integer.toString(btc_balance));
			}
			} catch (Exception e) {
				Log.v("Session_exception", e.getMessage());
			}
		}
	}
	
	public void finalizeBet(){
		if(this.isFinalized() == true) return;
		if(this.end_time_met) return;
		if(this.bet_key == "") return;
		if(this.spec_nr == "") return;
		this.setFinalized(true);
		new FinalizeBet().execute(new String[]{this.getGuid(), spec_nr, this.bet_key});
	}
	
	private class FinalizeBet extends AsyncTask<String, Void, Boolean>{

		@Override
		protected Boolean doInBackground(String... params) {
			Boolean ret = null;
			try {
				ret = RPBClient.FinalizeBet(params[0], params[1], params[2]);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		    return ret;
		}

		@Override
		protected void onPostExecute(Boolean result) { 
			try {
				if(result == true) {
					setFinalized(true);
					refreshRound();
					Log.v("GAME", "Finalization succeeded");
				}else{
					if(getActiveRound().getEndTime().compareTo(getActiveRound().getCurrentTime())>0){
						setFinalized(true);
						refreshRound();
					}
					Log.v("GAME", "Finalization failed");
					setFinalized(false);
				}
			} catch (Exception e) {
				Log.v("Finalize_exception", e.getMessage());
			}
		}
		
	}
	
	public void GetBTCTicket(String amount, String live){
		//TODO:
		String[] input;
		input = new String[5];
		input[0] = this.getGuid();
		input[1] = this.btc_addr;
		input[2] = amount;
		input[3] = live;
		input[4] = Game.TOKEN;
		
		new getBTCTicket().execute(input);
		
	}
	
	private class getBTCTicket extends AsyncTask<String, Void, Ticket>{

		@Override
		protected Ticket doInBackground(String... params) {
			Ticket ret = null;
			try {
				//guid, btc_addr, amount, live, token
				ret = RPBClient.getBTCTicket(params[0], params[1], params[2], params[3], params[4]);
			} catch (JSONException e) {
				Log.v("ERROR", e.getMessage());
				e.printStackTrace();
			}
		    return ret;
		}
		
		@Override
		protected void onPostExecute(Ticket result){
			game.setTicket(result);
			if(game.getTicket() == null) return;
			if(game.getTicket().isLive()){
				game.startWallet();
			}else{
				//Send test transaction for play money
				TestBTCTransaction(game.getTicket());
			}
		}
	}
	
	public void startWallet(){
		try{
			final StringBuilder uri = new StringBuilder("bitcoin:");
			uri.append(Ticket.BTC_ADDR);
			uri.append("?amount=").append(this.getTicket().getBTCAmount());
			Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(uri.toString()));
			this.context.startActivity(i);
		}catch(Exception ex){
			((MainActivity) context).show_wallet_not_found(Ticket.BTC_ADDR, this.getTicket().getBTCAmount());
		}
	}
	
	public void RenewBTCTicket(){
		new renewBTCTicket().execute("");
	}
	
	private class renewBTCTicket extends AsyncTask<String, Void, String>{

		@Override
		protected String doInBackground(String... params) {
			String ret = null;
			try {
				//guid, ticket_nr, token
				ret = RPBClient.renewBTCTicket(params[0], params[1], params[2]);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		    return ret;
		}
		
		@Override
		protected void onPostExecute(String result){
			
		}
	}
	
	public void GetBTCTransaction(){
		//TODO:
		new getBTCTransaction().execute("");
	}
	
	private class getBTCTransaction extends AsyncTask<String, Void, String>{

		@Override
		protected String doInBackground(String... params) {
			String ret = null;
			try {
				//guid, hash
				ret = RPBClient.getBTCTransaction(params[0], params[1]);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		    return ret;
		}
		
		@Override
		protected void onPostExecute(String result){
			
		}
	}
	
	public void SetBTCTransaction(){
		//TODO:
		new setBTCTransaction().execute("");
	}
	
	private class setBTCTransaction extends AsyncTask<String, Void, String>{

		@Override
		protected String doInBackground(String... params) {
			String ret = null;
			try {
				//guid, hash, token
				ret = RPBClient.setBTCTransaction(params[0], params[1], params[2]);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		    return ret;
		}
		
		@Override
		protected void onPostExecute(String result){
			
		}
	}
	
	public void TestBTCTransaction(Ticket ticket){
		if(this.session.getTestBalance_Combined() > 100) return;
		String[] input = new String[4];
		input[0] = this.getGuid();
		input[1] = ticket.amount;
		input[2] = "0";
		input[3] = Game.TOKEN;
		new testBTCTransaction().execute(input);
	}

	public boolean isFinalized() {
		return finalized;
	}
	public void setFinalized(boolean finalized) {
		this.finalized = finalized;
	}

	private class testBTCTransaction extends AsyncTask<String, Void, String>{

		@Override
		protected String doInBackground(String... params) {
			String ret = null;
			try {
				//guid, amount, scam, token
				ret = RPBClient.testBTCTransaction(params[0], params[1], params[2], params[3]);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		    return ret;
		}
		
		@Override
		protected void onPostExecute(String result){
			
		}
	}
}
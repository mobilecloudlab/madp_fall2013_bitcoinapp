package Core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.URI;
import java.net.UnknownHostException;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.util.Log;

public class RESTClient {

public String getGETRequest() throws Exception {

    BufferedReader in = null;
    String data = null;

    try {
        HttpClient client = new DefaultHttpClient();
        client.getConnectionManager().getSchemeRegistry().register(getMockedScheme());

        URI website = new URI("http://httpbin.org/ip"); 
        HttpGet request = new HttpGet();
        request.setURI(website);
        HttpResponse response = client.execute(request);
        response.getStatusLine().getStatusCode();

        in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuffer sb = new StringBuffer("");
        String l = "";
        String nl = System.getProperty("line.separator");
        while ((l = in.readLine()) != null) {
            sb.append(l + nl);
        }
        in.close();
        data = sb.toString();
        return data;
    } finally {
        if (in != null) {
            try {
                in.close();
                return data;
            } catch (Exception e) {
                Log.e("GetMethodEx", e.getMessage());
            }
        }
    }
}

public String getPOSTrequest(String fun, String[] params, JSONObject data) throws Exception {
	Log.v("POST", fun);
	String ret ="";
    BufferedReader in = null;

    try {
        HttpClient client = new DefaultHttpClient();
        client.getConnectionManager().getSchemeRegistry().register(getMockedScheme());

        URI website = new URI("https://www.hyena.net.ee/rockpaperbitcoin/database/"); 
        HttpPost request = new HttpPost();
        request.setURI(website);
        
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
        nameValuePairs.add(new BasicNameValuePair("fun", fun));
        nameValuePairs.add(new BasicNameValuePair("data", data.toString()));
        if(params.length > 0){
        	nameValuePairs.add(new BasicNameValuePair("sec_hash", params[0]));
        	nameValuePairs.add(new BasicNameValuePair("checksum", params[1]));
        	nameValuePairs.add(new BasicNameValuePair("token", params[2]));
        }
        request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        HttpResponse response = client.execute(request);
        response.getStatusLine().getStatusCode();

        in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuffer sb = new StringBuffer("");
        String l = "";
        String nl = System.getProperty("line.separator");
        while ((l = in.readLine()) != null) {
            sb.append(l + nl);
        }
        in.close();
        ret = sb.toString();
        return ret;
    } finally {
        if (in != null) {
            try {
                in.close();
                return ret;
            } catch (Exception e) {
                Log.e("HTTP_POST", e.getMessage());
            }
        }
    }
}

public Scheme getMockedScheme() throws Exception {
    MySSLSocketFactory mySSLSocketFactory = new MySSLSocketFactory();
    return new Scheme("https", mySSLSocketFactory, 443);
}

class MySSLSocketFactory extends SSLSocketFactory {
    javax.net.ssl.SSLSocketFactory socketFactory = null;

    public MySSLSocketFactory(KeyStore truststore) throws Exception {
        super(truststore);
        socketFactory = getSSLSocketFactory();
    }

    public MySSLSocketFactory() throws Exception {
        this(null);
    }

    @Override
    public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException,
            UnknownHostException {
        return socketFactory.createSocket(socket, host, port, autoClose);
    }

    @Override
    public Socket createSocket() throws IOException {
        return socketFactory.createSocket();
    }

    javax.net.ssl.SSLSocketFactory getSSLSocketFactory() throws Exception {
        SSLContext sslContext = SSLContext.getInstance("TLS");

        TrustManager tm = new X509TrustManager() {
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }
        };
        sslContext.init(null, new TrustManager[] { tm }, null);
        return sslContext.getSocketFactory();
    }
}
}

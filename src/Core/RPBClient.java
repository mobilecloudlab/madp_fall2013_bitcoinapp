package Core;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import DataClasses.Round;
import DataClasses.Session;
import DataClasses.Speculation;
import DataClasses.Ticket;
import android.util.Log;
public class RPBClient {
	
	public static String init(String guid) throws JSONException{
		RESTClient client = new RESTClient();      
	    String returned = null;
	    
	    JSONObject data = new JSONObject();
	    data.put("guid", guid);
	    try {
	        returned = client.getPOSTrequest("init", new String[]{}, data);

	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	        return returned;
	}

	public static Session getSession(String guid) throws JSONException{
		RESTClient client = new RESTClient();      
		Session ret = null;
	    
	    JSONObject data = new JSONObject();
	    data.put("guid", guid);
	    try {
	        String result = client.getPOSTrequest("get_session", new String[]{}, data);
	        JSONObject jr = new JSONObject(result);
	        ret = new Session(new JSONObject(jr.get("session").toString()));
	        //Log.v("POST_results", result);
	    } catch (Exception e) {
	    	Log.v("REST_ERROR", "getSession"); 
	        e.printStackTrace();
	    }
	        return ret;
	}

	public static String getLastRound(String guid) throws JSONException{
		RESTClient client = new RESTClient();      
	    String ret = null;
	    
	    JSONObject data = new JSONObject();
	    data.put("guid", guid);
	    try {
	    	ret = client.getPOSTrequest("get_last_round", new String[]{}, data);
	    } catch (Exception e) {
	    	Log.v("REST_ERROR", "getLastRound"); 
	    	e.printStackTrace();
	    }
	    return ret;
	}

	public static String getLastOpenRound(String guid) throws JSONException{
		RESTClient client = new RESTClient();      
	    String ret = null;
	    
	    JSONObject data = new JSONObject();
	    data.put("guid", guid);
	    data.put("min_time", "10");
	    data.put("max_time","3600");
	    try {
	    	ret = client.getPOSTrequest("get_last_open_round", new String[]{}, data);
	    	//Log.v("POST_results", ret);
	    } catch (Exception e) {
	    	Log.v("REST_ERROR", "getLastOpenRound"); 
	    	e.printStackTrace();
	    }
	    return ret;
	}
	
	public static String getNextRound(String guid, String live) throws JSONException{
		RESTClient client = new RESTClient();      
	    String roundNr ="";
	    
	    JSONObject data = new JSONObject();
	    data.put("guid", guid);
	    data.put("live", live);
	    try {
	        String result = client.getPOSTrequest("get_next_round", new String[]{}, data);
	        JSONObject JSONresult = new JSONObject(result);
	        roundNr = JSONresult.get("round_nr").toString();
	        //Log.v("POST_results", result);
	    } catch (Exception e) {
	    	Log.v("REST_ERROR", "getNextRound");
	    	e.printStackTrace();
	    }
	    return roundNr;
	}
	
	public static Round getRound(String guid, String RoundNr) throws JSONException{
		RESTClient client = new RESTClient();      
	    Round r = null;
	    JSONObject data = new JSONObject();
	    data.put("guid", guid);
	    data.put("round_nr", RoundNr);
	    try {
	    	String result = client.getPOSTrequest("get_round", new String[]{}, data);
	        JSONObject jr = new JSONObject(result);
	        r = new Round(new JSONObject(jr.get("round").toString()));
	        //Log.v("POST_results", result);
	    } catch (Exception e) {
	    	Log.v("REST_ERROR", "getRound");
	        e.printStackTrace();
	    }
	    return r;
	}
	
	public static String makeBet (String guid, String round_nr, String key, String live) throws JSONException{
		RESTClient client = new RESTClient();      
	    String ret = null;
	    
	    JSONObject data = new JSONObject();
	    data.put("guid", guid);
	    data.put("round_nr", round_nr);
	    data.put("hash", Utilities.keyToHash(key));
	    data.put("live", live);
	    try {
	        ret = client.getPOSTrequest("make_bet", new String[]{}, data);
	        //Log.v("POST_results", ret);
	    }catch (Exception e) {
	    	Log.v("REST_ERROR", "makeBet_ERROR");
	        e.printStackTrace();
	    }
	    return ret;
	}
		
	public static Boolean changeBet(String guid, String spec_nr,String key) throws JSONException{
		RESTClient client = new RESTClient();      
	    String result = null;
	    
	    JSONObject data = new JSONObject();
	    data.put("guid", guid);
	    data.put("nr", spec_nr);
	    data.put("hash", Utilities.keyToHash(key));
	    try {
	        result = client.getPOSTrequest("change_bet", new String[]{}, data);
	        //Log.v("POST_results", result);
	    }catch (Exception e) {
	    	Log.v("REST_ERROR", "changeBet");
	        e.printStackTrace();
	    }
	    JSONObject jr = new JSONObject(result);
	    String ret = (String) jr.get("result");
	    if(ret == "SUCCESS"){
	    	return true;
	    }else{
	    	return false;
	    }
	}
	
	public static List<Speculation> getSpeculations(String guid, String round_nr) throws JSONException{
		//get_speculations
		RESTClient client = new RESTClient();      
		List<Speculation> ret = new ArrayList<Speculation>();
	    
	    JSONObject data = new JSONObject();
	    data.put("guid", guid);
	    data.put("round_nr", round_nr);
	    try {
	        String result = client.getPOSTrequest("get_speculations", new String[]{}, data);
	        JSONObject jr = new JSONObject(result);
	        JSONArray ja = jr.getJSONArray("speculations");
	        
	        for (int i = 0; i < ja.length(); i++) {
				ret.add(new Speculation((JSONObject)ja.get(i)));
			}
	        //Log.v("POST_results", result);
	    } catch (Exception e) {
	    	Log.v("REST_ERROR", "getSpeculations");
	        e.printStackTrace();
	    }
	    return ret;
	}
	
	public static Boolean FinalizeBet(String guid, String speculation_nr, String choice) throws JSONException{
				RESTClient client = new RESTClient();   
				String result = "";
			    
			    JSONObject data = new JSONObject();
			    data.put("guid", guid);
			    data.put("nr", speculation_nr);
			    data.put("key", choice);
			    try {
			        result = client.getPOSTrequest("finalize_bet", new String[]{}, data);
			        //Log.v("POST_results", result);
			    } catch (Exception e) {
			    	Log.v("REST_ERROR", e.getMessage());
			    	e.printStackTrace();
			    }  
			    JSONObject jr = new JSONObject(result);
			    String ret = (String) jr.get("result");
			    if(ret.equals("SUCCESS")){
			    	return true;
			    }else{
			    	return false;
			    }
	}

	public static Ticket getBTCTicket(String guid, String btc_addr, String amount, String live, String token) throws JSONException{
		RESTClient client = new RESTClient();   
		String result = "";
	    
	    JSONObject data = new JSONObject();
	    data.put("guid", guid);
	    data.put("btc_addr", btc_addr);
	    data.put("amount", amount);
	    data.put("live", live);
	    data.put("token", token);
	    try {
	        result = client.getPOSTrequest("get_btc_ticket", new String[]{}, data);
	        //Log.v("POST_results", result);
	    } catch (Exception e) {
	    	Log.v("REST_ERROR", "get_btc_ticket");
	    	e.printStackTrace();
	    }  
        JSONObject jr = new JSONObject(result);
        JSONObject ja = jr.getJSONObject("btc_ticket");
		return new Ticket(ja);
	}

	public static String renewBTCTicket(String guid, String ticket_nr, String token) throws JSONException{
		RESTClient client = new RESTClient();   
		String result = "";
	    
	    JSONObject data = new JSONObject();
	    data.put("guid", guid);
	    data.put("ticket_nr", ticket_nr);
	    data.put("token", token);
	    try {
	        result = client.getPOSTrequest("renew_btc_ticket", new String[]{}, data);
	        //Log.v("POST_results", result);
	    } catch (Exception e) {
	    	Log.v("REST_ERROR", "renew_btc_ticket");
	    	e.printStackTrace();
	    }  
	    JSONObject jr = new JSONObject(result);
	    String ret = (String) jr.get("result");
		return ret;
	}
	
	public static String getBTCTransaction(String guid, String hash) throws JSONException{
		RESTClient client = new RESTClient();   
		String result = "";
	    
	    JSONObject data = new JSONObject();
	    data.put("guid", guid);
	    data.put("hash", hash);
	    try {
	        result = client.getPOSTrequest("get_btc_tx", new String[]{}, data);
	        //Log.v("POST_results", result);
	    } catch (Exception e) {
	    	Log.v("REST_ERROR", "get_btc_tx");
	    	e.printStackTrace();
	    }  
	    JSONObject jr = new JSONObject(result);
	    String ret = (String) jr.get("result");
		return ret;
	}

	public static String setBTCTransaction(String guid, String hash, String token) throws JSONException{
		RESTClient client = new RESTClient();   
		String result = "";
	    
	    JSONObject data = new JSONObject();
	    data.put("guid", guid);
	    data.put("hash", hash);
	    data.put("token", token);
	    try {
	        result = client.getPOSTrequest("set_btc_tx", new String[]{}, data);
	        //Log.v("POST_results", result);
	    } catch (Exception e) {
	    	Log.v("REST_ERROR", "set_btc_tx");
	    	e.printStackTrace();
	    }  
	    JSONObject jr = new JSONObject(result);
	    String ret = (String) jr.get("result");
		return ret;
	}
	
	public static String testBTCTransaction(String guid, String amount, String scam, String token) throws JSONException{
		RESTClient client = new RESTClient();   
		String result = "";
	    
	    JSONObject data = new JSONObject();
	    data.put("guid", guid);
	    data.put("amount", amount);
	    data.put("scam", scam);
	    data.put("token", token);
	    try {
	        result = client.getPOSTrequest("test_btc_tx", new String[]{}, data);
	        //Log.v("POST_results", result);
	    } catch (Exception e) {
	    	Log.v("REST_ERROR", "test_btc_tx_error" + e.getMessage());
	    	e.printStackTrace();
	    }  
	    JSONObject jr = new JSONObject(result);
	    String ret = (String) jr.get("result");
		return ret;
	}
}




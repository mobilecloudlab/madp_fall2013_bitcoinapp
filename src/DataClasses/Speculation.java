package DataClasses;

import org.json.JSONObject;

public class Speculation {
private int nr, session_nr, round_nr, live, dropped;
private String hash, key;

public Speculation(JSONObject data){
	try {
		this.nr = data.getInt("nr");
		this.session_nr = data.getInt("session_nr");		
		this.round_nr = data.getInt("round_nr");
		this.live = data.getInt("live");
		this.dropped = data.getInt("dropped");
		this.hash = data.getString("hash");
		this.key = data.getString("key");
		
	} catch (Exception e) {
		// TODO: handle exception
	}
}
public int getSpecultaionNr(){
	return this.nr;
}
public String getHash(){
	return this.hash;
}

public String getKey(){
	return this.key;	
}
public String toString(){
	return "Specultaion:" + this.nr +"\n"+
			"Session_nr:" + this.session_nr +"\n"+
			"Round_nr:" + this.round_nr +"\n"+
			"Live:" + this.live +"\n"+
			"Dropped:" + this.dropped +"\n"+
			"Hash:" + this.hash +"\n"+
			"Key:"+this.key;
}
}

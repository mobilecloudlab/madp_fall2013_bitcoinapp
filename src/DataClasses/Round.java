package DataClasses;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.JSONObject;

import android.util.Log;

public class Round {
	private int nr = 0, bets, pot, invalidBets, rewardRock, rewardPaper, forfeit, 
				rewardBitcoin, maxbets, open, live, duration,invalidPot, validPot, potRock, potPaper,
				potBitcoin, bonus, bonusFee, countRocks, countPapers, countBitcoins, client_stake = 0;
	
	private boolean confirmed, result;
	private Date startTime, keyTime, endTime, lastUpdate, lastRequest, currentTime;
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH); 
	
	public Round(String round_nr){
		if(round_nr == null){
			return;
		}else{
			try{
				this.nr = Integer.parseInt(round_nr);
			}catch(Exception ex){
				
			}
		}
	}
	
	public Round(JSONObject round){
		try {
			this.lastRequest = new Date();
			this.nr = Integer.parseInt(round.getString("nr").toString());
			this.bets = Integer.parseInt(round.getString("bets").toString());
			this.open = Integer.parseInt(round.getString("open").toString());
			this.setLive(Integer.parseInt(round.getString("live").toString()));
			this.confirmed = Boolean.parseBoolean(round.getString("confirmed").toString());
			this.forfeit = Integer.parseInt(round.getString("forfeit").toString());
			this.startTime = new Date();
			if(!round.getString("start_time").equals("null")){
				this.startTime = dateFormat.parse(round.getString("start_time"));
			}
			if(!round.getString("key_time").equals("null")){
				this.keyTime = dateFormat.parse(round.getString("key_time"));
			}
			if(!round.getString("end_time").equals("null")){
				this.endTime = dateFormat.parse(round.getString("end_time"));
			}
			if(!round.getString("current_time").equals("null")){
				this.setCurrentTime(dateFormat.parse(round.getString("current_time")));
			}
			this.lastUpdate = new Date();
			if(!round.getString("last_update").equals("null")){
				this.lastUpdate = dateFormat.parse(round.getString("last_update"));
			}
			if(round.getString("pot") != "null"){
				this.pot = Integer.parseInt(round.getString("pot"));
			}else{
				this.pot = 0;
			}
			if(round.getString("invalid_bets") != "null"){
				this.invalidBets = Integer.parseInt(round.getString("invalid_bets"));
			}else{
				this.invalidBets = 0;
			}
			if(round.getString("invalid_pot") != "null"){
				this.invalidPot = Integer.parseInt(round.getString("invalid_pot"));
			}else{
				this.invalidPot = 0;
			}
			if(round.getString("valid_pot") != "null"){
				this.validPot = Integer.parseInt(round.getString("valid_pot"));
			}else{
				this.validPot = 0;
			}
			if(round.getString("live") != "null"){
				this.live = Integer.parseInt(round.getString("live"));
			}else{
				this.live = 0;
			}
		}catch (Exception e) {
			Log.v("GAME", "Round_full_constructor");
			e.printStackTrace();
		}
		
	}

//	public void refreshRound(Round round){
//		try {
//			this.lastRequest = new Date();
//			this.nr = round.getRoundNr();
//			this.bets = round.getBets();
//			this.open = round.getOpen();
//			this.live = round.getLive();
//			this.forfeit = round.getForfeit();
//			this.keyTime = round.getKeyTime();
//			this.endTime = round.getEndTime();
//			this.currentTime = round.getCurrentTime();
//			this.pot = round.getPotSize();
//			this.invalidBets = round.getInvalidBets();
//			this.live = round.getLive();
//
//		}catch (Exception e) {
//			Log.v("GAME", "round refresh");
//			e.printStackTrace();
//		}
//		
//	}
	
	public String toString(){
		String start = "",
				end = "", 
				update = "",
				lastRequest ="";
		try{
			lastRequest = dateFormat.format(this.lastRequest);
			start = dateFormat.format(this.startTime);
			end = dateFormat.format(this.endTime);
			update = dateFormat.format(this.lastUpdate);
		}
		catch(Exception ex){
			
		}

		return "Round:" + Integer.toString(this.nr) + "\n" +
				"Bets:" + Integer.toString(this.bets) + "\n" +
				"Pot:" + Integer.toString(this.pot) + "\n" +
				"Start:" + start + "\n" +
				"End:"+ end + "\n" +
				"LastUpdate:"+ update + "\n"+
				"LastRequest:"+ lastRequest + "\n";
			
		
	}

	public Integer getRoundNr(){
		return this.nr;
	}

	public int getForfeit(){
		return this.forfeit;
	}
	
	public int getPotSize(){
			return this.bets * this.forfeit;
	}
	
	public int getBets(){
		return this.bets;
	}
	
	public int getInvalidBets(){
		return this.invalidBets;
	}
	
	public int getRewardRock() {
		return this.rewardRock;
	}
	
	public int getRewardPaper() {
		return this.rewardPaper;
	}
	
	public int getRewardBitcoin() {
		return this.rewardPaper;
	}
	
	public Date getLastReferesh() {
		return this.lastRequest;
	}

	public Date getEndTime(){
		return this.endTime;
	}
	
	public Date getKeyTime(){
		return this.keyTime;
	}

	public int getLive() {
		return live;
	}

	public void setLive(int live) {
		this.live = live;
	}

	public Date getCurrentTime() {
		return currentTime;
	}

	public int getOpen()
	{
		return this.open;
	}
	
	public void setCurrentTime(Date currentTime) {
		this.currentTime = currentTime;
	}

	public int getClient_stake() {
		return this.client_stake;
	}

	public void setClient_stake(int client_stake) {
		this.client_stake = client_stake;
	}

}

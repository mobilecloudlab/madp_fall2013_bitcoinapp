#Rock Paper Bitcoin - Android client
by **Erich Erstu** and **Joosep Püüa**

##Abstract

The aim of this project is to develop a user friendly and simplistic native user
interface for a Rock-Paper-Scissors (RPS) game that can be played on bitcoins.
The game server provides a RESTful API that is to be used when developing
the client.

public API used by client is here: [API][3]
YouTube video of the app is provided here: [RPB in action][2]

##Importing RPB into eclipse: (the easy way)
1. Download the solution
2. Import downloaded solution into the eclipse as an existing project.
3. And your done.

##Building the solution with ant: (more complex way)
1. Download ant (if you don't have it yet) [ant install][1]
2. Download the solution
3. Open your cmd or terminal
4. Use ant commands to build app using provided build.xml file

###Build for release
	ant -buildfile path/to/provided/build.xml release
###Build for debug
	ant -buildfile path/to/provided/build.xml debug
	
	
	
[1]:http://ant.apache.org/manual/install.html
[2]:http://youtu.be/wRaj17zrYpA
[3]:http://www.rockpaperbitcoin.com/API
	
